import { Component, OnInit } from '@angular/core';
import { Home } from './Models/home';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public home: Home;
  inputText: string = '';
  message: string = '';


  constructor() {
    this.home = {
      intro: {
        img:
          'https://geoinnova.org/blog-territorio/wp-content/uploads/2020/09/comida-ambiente-dieta-2-300x200.jpg',
        description:
          'El vínculo entre comida y ambiente es claro en muchos aspectos. En este oportunidad, veremos cómo optar por una dieta basada en alimentos saludables reduciría los impactos negativos en la naturaleza. En artículos anteriores hemos hablado sobre comer de una forma más sostenible. La gastronomía sostenible promueve hábitos respetuosos con el ambiente y el comercio.',
      },

      gallery: {
        img: [
          {
            img:
              'https://t1.rg.ltmcdn.com/es/images/4/0/0/huevos_al_plato_al_horno_75004_600.jpg',
            descritpion:
              'Los huevos al plato son uno de los platos más tradicionales de la gastronomía española. Normalmente, se preparan en sartén, pero también podemos hacerlo al horno sin perder sabor ni textura. Así mismo, los ingredientes pueden variar en función de la región. En este caso, haremos huevos al plato al horno con jamón serrano, que se puede sustituir por jamón dulce, chorizo o bacon. Eso sí, lo que no debe faltar es el tomate, los guisantes y, por supuesto, el huevo, ya que son los ingredientes básicos de la receta. https://www.recetasgratis.net/receta-de-huevos-al-plato-al-horno-75004.html',
          },
          {
            img:
              'https://t1.rg.ltmcdn.com/es/images/8/9/9/churrasco_argentino_73998_600.jpg',
            descritpion:
              'El churrasco no es más que un bife o filete de ternera o de cerdo sin hueso. Por lo general, se prepara en la plancha caliente, aunque también puede hacerse a la parrilla y sobre las brasas. No es un bistec muy ancho, pues debe cortarse de aproximadamente 2 centímetros y cocinarse vuelta y vuelta.',
          },
        ],
      },
    };
  }

  ngOnInit(): void {}

  keyUp(letra: string) {
    this.inputText = letra;}

  introMessage(message: string){
    console.log(message)
  }
}
