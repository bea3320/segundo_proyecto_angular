import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Intro } from '../models/home';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss'],
})
export class IntroComponent implements OnInit {
  @Input() intro!: Intro;

  @Output() emitMessage = new EventEmitter<string>();

  message: string = '';

  constructor() {}

  ngOnInit(): void {}

  keyUp(letra: string) {
    this.message = letra;
    console.log(this.message);
  }
  sendMessage() {
    this.emitMessage.emit(this.message);
  }
}
