export interface Home {
  intro: Intro;
  gallery: Gallery;
}

export interface Intro {
  img: string;
  description: string;
}

export interface Gallery{
  img: Array<Image>;
}

export interface Image{
  img: string;
  descritpion: string;
}
