export interface Header {
  icon: Image,
  links:Array<Links>
}

export interface Image{
  img:string,
  description: string
}

export interface Links{
  linkText: string
}
