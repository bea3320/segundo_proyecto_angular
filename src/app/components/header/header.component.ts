import { Component, OnInit } from '@angular/core';
import { Header } from './Models/header';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public header: Header;
  constructor() {
    this.header = {
      icon: {
        img: 'https://i.pinimg.com/originals/21/84/c8/2184c86ec8cb5ed091c33a7d5a74d8a7.png',
        description: 'Recetas de cocina',
      },
      links: [
        {
          linkText: 'Aprende a Cocinar',
        },
        {
          linkText: 'Recetas Fáciles'
        }
      ]
      }
    }


  ngOnInit(): void {}
}

