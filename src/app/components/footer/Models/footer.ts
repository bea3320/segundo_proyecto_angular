export interface Footer {
  links:Array<Links>
}

export interface Links{
  linkText: string;
  link: string
}
