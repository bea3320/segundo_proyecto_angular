import { stringify } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { Footer } from './Models/footer';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
public footer: Footer;
  constructor() {
    this.footer={
      links:
      [
        {
          linkText: 'Condiciones de Uso',
          link:'https://www.pequerecetas.com/condiciones-uso/',
        },
        {
          linkText: 'Cookies',
          link:'https://www.pequerecetas.com/politica-de-cookies/',
        },
        {
          linkText: 'Privacidad y Aviso Legal',
          link:'https://www.pequerecetas.com/politica-privacidad/',
        }
      ]
    }
  }

  ngOnInit(): void {
  }

}
